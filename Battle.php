<?php

/**
 * Created by PhpStorm.
 * User: GabrielAlexandru
 * Date: 18-Mar-17
 * Time: 11:42 AM
 */
class Battle
{

    private $orderus;
    private $enemy;
    private $turns;

    private $currentAttacker;
    private $currentDefender;
    private $damage;

    public function __construct(Warrior $orderus, Warrior $enemy)
    {
        $this->orderus  = $orderus;
        $this->beast    = $enemy;
        $this->turns    = 20;
    }

    /**
     * The battle will end when one of the
     * fighters reaches 0 health or when the
     * maximum number of turns is reached.
     */
    public function battle()
    {
        
        $this->init();

        while(($this->turns > 0) && ($this->orderus->health > 0) && ($this->beast->health > 0))
        {
            $luck = $this->fight();

            /**
             * Decrement the turns
             */
            $this->turns--;

            echo "Turn: ".(20 - $this->turns). "<br>";
            echo "<b>".$this->currentDefender->name."</b> attacked <br>";

            if($luck)
            {
                echo $this->currentAttacker->name . " is lucky <br>";
            }

            echo "Orderus health: ".$this->orderus->health."<br>";
            echo "Wild Beast health: ".$this->beast->health."<br>";
            echo "Damage: ". $this->damage."<br>";
            echo "______________________________<br>";
        }
   
    }

    /**
     * Initiate the battle
     * The first attack is made by the fastest
     * fighter or the luckiest fighter.
     */

    private function init()
    {
        if($this->orderus->speed == $this->beast->speed)
        {
            $this->currentAttacker = ($this->orderus->luck > $this->beast->luck) ? $this->orderus : $this->beast;
        }
        else
        {
            $this->currentAttacker = ($this->orderus->speed > $this->beast->speed) ? $this->orderus : $this->beast;
        }

        $this->currentDefender = $this->getOposite($this->orderus, $this->beast, $this->currentAttacker);
    }


    /**
     * Get the oposite fighter of the
     * current attacker
     */
    private function getOposite($attacker, $defender, $currentAttacker)
    {
        if($currentAttacker->name == $attacker->name)
        {
            return $defender;
        }
        elseif
        ($currentAttacker->name == $defender->name)
        {
            return$attacker;
        }
        else
        {
            return $currentAttacker;
        }
    }

    /**
     * Check if the warrior got luck
     * Create the attack action
     * Create the defend action
     * Change the Attacker
     */
    private function fight()
    {
        $luck = FALSE;

        if(!$this->checkProbability($this->currentDefender->luck))
        {
            $this->attack();
            $this->defend();
            $this->calculateHealth($this->damage);
        }
        else
        {
            $luck = TRUE;
        }

        $this->currentDefender = $this->currentAttacker;
        $this->currentAttacker = $this->getOposite($this->orderus, $this->beast, $this->currentAttacker);

        return $luck;

    }

    /**
     * Create the attack action
     */

    private function attack()
    {
        $this->damage = $this->currentAttacker->attack($this->currentAttacker->strength, $this->currentDefender->defence);
    }

    /**
     * Create the defend action
     */
    private function defend()
    {
        $this->damage = $this->currentDefender->defend($this->damage);
    }

    /**
     * Calculate the remaining health
     * after the battle
     */
    private function calculateHealth($damage)
    {
        $this->currentDefender->health -= $damage;
    }

    /**
     * Check the probability of getting an attack
     */
    private function checkProbability($chance)
    {
        if(rand(1,100) <= $chance)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

}