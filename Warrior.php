<?php

/**
 * Created by PhpStorm.
 * User: GabrielAlexandru
 * Date: 18-Mar-17
 * Time: 11:03 AM
 */
abstract class Warrior
{

    public $name;
    public $strenght;
    public $health;
    public $defence;
    public $speed;
    public $luck;


    /**
     * Warrior constructor.
     * @param $params - that will contain
     * the stats of a fighter
     */
    public function __construct($params)
    {
        foreach($params as $key=>$value)
        {
            $this->$key = $value;
        }

        $this->set_name();
    }


    /**
     * This function will calculate the attack damage
     * by substracting the $attackerDefence from the $attackerStrenght
     * @param $attackerStrenght
     * @param - $attackerDefence
     * @return - the damage
     */
    public function attack($attackerStrength, $attackerDefence)
    {
        return (int)($attackerStrength - $attackerDefence);
    }


    /**
     * This is the defend function
     */
    public function defend($damage)
    {
        return $damage;
    }


    /**
     * Set the name of the fighter.
     */
    abstract public function set_name();


}