<?php
include_once ('Warrior.php');
/**
 * Created by PhpStorm.
 * User: GabrielAlexandru
 * Date: 18-Mar-17
 * Time: 11:17 AM
 */
class Orderus extends Warrior
{
    /**
     * Define the probability to attack
     * or defend with a special skill
     */
    private $rapidChance = 10;
    private $shieldChance = 20;



    public function attack($attackerStrenght, $defenderStrenght)
    {
        /**
         * Calculate the damage
         */
        $damage = parent::attack($attackerStrenght, $defenderStrenght);

        /**
         * Check if the hero has used RapidStrike.
         */
        if($this->rapidStrike())
        {
           echo 'Rapid Strike<br>';
           $damage *=2;
        }

        return $damage;

    }


    public function defend($damage)
    {
        /**
         * Check if the hero has used MagicShield
         */

        if($this->magicShield())
        {
            echo 'Magic Shield<br>';
            $damage /=2;
        }
        return $damage;
    }


    /**
     * Create the Hero Skills
     * Rapid Strike - rapidStrike()
     * Magic Shield - magicShield()
     */

    /**
     * Rapid Strike will be used with a 10%
     * chance to activate.
     * the hero will hit twice
     * @return bool
     */
    private function rapidStrike()
    {
        /**
         * Check the probability of using rapidStrike
         */

        if(rand(1,100) <= $this->rapidChance)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    /**
     * Magic Shield will be used with a 20%
     * chance to activate.
     * The hero will take half the damage when
     * attacked by an enemy;
     * @return bool
     */
    private function magicShield()
    {
        /**
         * Check the probability of using magicShield
         */

        if(rand(1,100) <= $this->shieldChance)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }


    /**
     * Set the name of our Hero
     */
    public function set_name()
    {
        $this->name = 'Orderus';
    }
}