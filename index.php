<?php
include "Orderus.php";
include "Enemy.php";
include "Battle.php";
/**
 * Created by PhpStorm.
 * User: GabrielAlexandru
 * Date: 18-Mar-17
 * Time: 12:23 PM
 */

$params = array(
    'orderus' => array(
        'health' => array(
            'min' => 70,
            'max' => 100
        ),
        'strength' => array(
            'min' => 70,
            'max' => 80
        ),
        'defence' => array(
            'min' => 45,
            'max' => 55
        ),
        'speed' => array(
            'min' => 40,
            'max' => 50
        ),
        'luck' => array(
            'min' => 10,
            'max' => 30
        ),
    ),
    'beast' => array(
        'health' => array(
            'min' => 60,
            'max' => 90
        ),
        'strength' => array(
            'min' => 60,
            'max' => 90
        ),
        'defence' => array(
            'min' => 40,
            'max' => 60
        ),
        'speed' => array(
            'min' => 40,
            'max' => 60
        ),
        'luck' => array(
            'min' => 25,
            'max' => 40
        )
    )
);

/**
 * Generate random values for fighters properties
 */

foreach($params as $warrior => $param) {
    foreach($param as $key =>$value) {
        $params[$warrior][$key] = getRand($value['min'], $value['max']);
    }
}

/**
 * getRand return random value between a min and max inputs
 * return value between min and max
 * */
function getRand($start, $stop) {
    return rand($start, $stop);
}


$orderus = new Orderus($params['orderus']);
$beast = new Enemy($params['beast']);

$battle = new Battle($orderus, $beast);
$battle->battle();
