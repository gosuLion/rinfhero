<?php
include_once ('Warrior.php');
/**
 * Created by PhpStorm.
 * User: GabrielAlexandru
 * Date: 18-Mar-17
 * Time: 11:40 AM
 */
class Enemy extends Warrior
{

    /**
     * Set the name of the enemy
     */
    public function set_name()
    {
     $this->name = 'Wild Beast';
    }
}